#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "budgetitem.h"

#include <QList>
#include <QDate>
#include <QDebug>
#include <QKeyEvent>
#include <QSqlError>
#include <filesystem>
#include <QTableWidgetItem>


int counter{1};
int connection{0};
int id{-1};
int runOnce{0};
int mode{0};
int deleteCounter{0};

QSqlDatabase db;

QList<int> idList;
QList<bool> checkList;
bool check;

QList<BudgetItem*> BudgetItems;
BudgetItem budgetItem;
QDate qdate{QDate::currentDate()};
QString date;
QStringList monthList {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    qDebug() << QString::fromStdString(std::filesystem::current_path());

    ui->stackedWidget->setCurrentIndex(0);
    ui->comboBox->setDisabled(true);

    ui->pushNext->setText("Connect to database");

    ui->pushBack->setText("Exit");

    ui->dateEditDateAdd->setDisabled(true);

    ui->monthBudgets->setDisabled(true);
    ui->comboBoxEditMonths->setDisabled(true);

    connect(ui->pushNext, SIGNAL (released()), this, SLOT (pushNext_released()));
    connect(ui->pushBack, SIGNAL (released()), this, SLOT (pushBack_released()));

    //Budgets
    ui->lineEditBudgetTotal->setReadOnly(true);

    //Add entries
    connect(ui->checkBoxMonthly, SIGNAL(released()), this, SLOT (checkBoxMonthly_released()));
    connect(ui->checkBoxCustomDate, SIGNAL(released()), this, SLOT (checkBoxCustomDate_released()));
    ui->dateEditDateAdd->setDate(qdate);
    ui->addEntriesChild->setCurrentIndex(0);

    //Edit entries
    connect(ui->comboBoxEditMonths, SIGNAL(activated(int)), this, SLOT(comboBoxEditMonths_activated()));
    connect(ui->pushButtonDeleteYes, SIGNAL(released()), this, SLOT(pushButtonDeleteYes_released()));
    connect(ui->pushButtonDeleteNo, SIGNAL(released()), this, SLOT(pushButtonDeleteNo_released()));
    connect(ui->checkBoxEditMonthly, SIGNAL(stateChanged(int)), this, SLOT(checkBoxEditMonthly_stateChanged()));
    ui->editEntriesChild->setCurrentIndex(0);
    ui->comboBoxEditEntries->setDisabled(true);
    ui->dateEditDateEdit->setDisplayFormat("dd-MM-yyyy");
    ui->pushButtonDeleteNo->hide();


    emit pushNext_released();
}

MainWindow::~MainWindow()
{
    delete ui;
}
//Key Event
void MainWindow::keyPressEvent(QKeyEvent *k)
{
    if(k->key() == Qt::Key_Return || k->key() == Qt::Key_Enter)
    {
        emit pushNext_released();
    }
    else if(k->key() == Qt::Key_Escape)
    {
        if(ui->stackedWidget->currentIndex() > 0)
        {
            emit pushBack_released();
        }
        else return;
    }
    else return;
}

//My Functions
auto checkName(QString input) -> bool
{
    for(int i{0}; i <= input.length(); i++){
        if(input[i].isNumber()){
            return false;
        }
        else;
    }
    if(input.isEmpty()){
        return false;
    }
    else return true;
}

auto checkValue(QString input) -> bool
{
    bool a;
    input.toDouble(&a);
    if(a == true) return true;
    else return false;
}

auto checkDate(QString input, int mode) -> bool
{
    int number{0};
    int numbers[]{32, 13, 100};
    if(mode == 1)
    {
        for(int i{0}; i < 3; i++)
        {
            number = input.section(".", i, i).toInt();
            if(number < numbers[i]);
            else return false;
        }
        return true;
    }
    else if(mode == 2)
    {
        bool check;
        input.toInt(&check);
        if(check == true){
            if(input.toInt() < 32) return true;
            else return false;
        }
        else return false;
    }
    return false;
}

QString MainWindow::dateMonthAndYear(QString input)
{
    //Get only month and year from full date
    QString output{""};
    output += input.section("-", 1, 1) + "-" + input.section("-", 0, 0);
    return output;
}
QString MainWindow::dateISOConvertMonthAndYear(QString input)
{
    //Convert month and year to ISO
    QString output{""};
    output += input.section("-", 1, 1) + "-" + input.section("-", 0, 0);
    return output;
}

QString MainWindow::dateConvertFromISO(QString input)
{
    return (input.section("-", 2, 2) + "-" + input.section("-", 1, 1) + "-" + input.section("-", 0, 0));
}

QString MainWindow::dateISODaysInMonth(QString dateISO)
{
    QCalendar output;
    return QString::number(output.daysInMonth(dateISO.section("-", 1, 1).toInt(), dateISO.section("-", 0, 0).toInt()));
}

void MainWindow::clearUI()
{

    ui->outputSender->clear();

    ui->tableWidgetBudgetMain->clear();
    ui->tableWidgetBudgetMain->setRowCount(0);

    ui->monthBudgets->setDisabled(true);
    ui->comboBoxEditMonths->setDisabled(true);

    ui->progressBar->setValue(0);

    //Budgets
    ui->monthBudgets->clear();

    //add entries
    ui->addEntriesChild->setCurrentIndex(0);
    ui->dateEditDateAdd->clear();
    ui->lineEditName->clear();
    ui->lineEditValue->clear();
    ui->dateEditDateAdd->setDate(qdate);

    //edit entries
    runOnce = 0;
    ui->editEntriesChild->setCurrentIndex(0);
    ui->pushButtonDeleteNo->hide();
    ui->labelDelete->clear();
    ui->comboBoxEditMonths->clear();
    ui->comboBoxEditMonths->setDisabled(true);
    ui->comboBoxEditMonths->addItem("Pick a month");
    ui->comboBoxEditEntries->clear();
    ui->comboBoxEditEntries->setDisabled(true);
}
//Slots
//Add Entries slots
void MainWindow::checkBoxMonthly_released()
{
    if(ui->checkBoxMonthly->isChecked() == true)
    {
        ui->dateEditDateAdd->setDisplayFormat("dd");
    }
    else ui->dateEditDateAdd->setDisplayFormat("dd-MM-yyyy");
}

void MainWindow::checkBoxCustomDate_released()
{
    if(ui->checkBoxCustomDate->isChecked() == true)
    {
        ui->dateEditDateAdd->setEnabled(true);
    }
    else ui->dateEditDateAdd->setDisabled(true);
}

//Edit Entries slots
void MainWindow::comboBoxEditMonths_activated()
{
    if(runOnce == 0)
    {
        ui->comboBoxEditMonths->removeItem(0);
        runOnce = 1;
    }
    ui->comboBoxEditEntries->setEnabled(true);
    ui->comboBoxEditEntries->clear();
    QString selected{ui->comboBoxEditMonths->currentText()};
    QString entry{""};
    std::pair<bool, QSqlQuery> fetchEntries;
    idList.clear();
    //Fetching entries
    if(selected == "Monthly")
    {
        fetchEntries = budgetItem.findEntriesMonthly(db);
        if(fetchEntries.first == true){
            while (fetchEntries.second.next()) {
                for(int i{0}; i < 4; i++){
                    if(i == 0)
                    {
                        idList.append(fetchEntries.second.value(i).toInt());
                    }
                    else entry += fetchEntries.second.value(i).toString() + " ";
                }
                entry = entry.trimmed();
                ui->comboBoxEditEntries->addItem(entry);
                entry = "";
            }
        }
    }
    else
    {
        selected = dateISOConvertMonthAndYear(selected);
        fetchEntries = budgetItem.findEntries(db, (selected + "-01"), (selected + "-" + dateISODaysInMonth(selected)));
        if(fetchEntries.first == true){
            while (fetchEntries.second.next()) {
                for(int i{0}; i < 4; i++){
                    if(i == 0)
                    {
                        idList.append(fetchEntries.second.value(i).toInt());
                    }
                    else entry += fetchEntries.second.value(i).toString() + " ";
                }
                entry = entry.trimmed();
                ui->comboBoxEditEntries->addItem(entry);
                entry = "";
            }
        }
        else
        {
            qDebug() << "Send Data error:"
                     << fetchEntries.second.lastError()
                     << "\n"
                     << fetchEntries.second.lastQuery()
                     << "\n"
                     << fetchEntries.second.executedQuery();
        }
    }

}

void MainWindow::pushButtonDeleteYes_released()
{
    if(deleteCounter == 0)
    {
        deleteCounter++;
        ui->pushButtonDeleteNo->show();
        ui->labelDelete->setText("Sure?");
    }
    else if(deleteCounter == 1)
    {

        std::pair<bool, QSqlQuery> removeEntry = budgetItem.removeEntry(db, id, mode);
        if(removeEntry.first == false)
        {
            ui->outputSender->setText("Could not delete entry in the database, check debug log");
            qDebug() << "Send Data error:"
                     << removeEntry.second.lastError()
                     << "\n"
                     << removeEntry.second.lastQuery()
                     << "\n"
                     << removeEntry.second.executedQuery();
        }
        else
        {
            clearUI();
            ui->stackedWidget->setCurrentIndex(0);
            ui->pushBack->setText("Exit");
            ui->outputSender->setText("Successfully deleted entry");
            deleteCounter = 0;
        }
    }
}

void MainWindow::pushButtonDeleteNo_released()
{
    deleteCounter = 0;
    ui->labelDelete->clear();
    ui->pushButtonDeleteNo->hide();
}

void MainWindow::checkBoxEditMonthly_stateChanged()
{
    if(ui->checkBoxEditMonthly->isChecked() == true)
    {
        ui->dateEditDateEdit->setDisplayFormat("dd");
    }
    else ui->dateEditDateEdit->setDisplayFormat("dd-MM-yyyy");


}

//Push Buttons
void MainWindow::pushNext_released()
{
    //First connect
    if(connection == 0){
        db = budgetItem.databaseConnection(db);
        if(db.open() == true){
            connection = 1;
            ui->outputSender->setText("You are now connected to the database");
            ui->pushNext->setText("Next");
            ui->comboBox->setDisabled(false);
        }
        else{
            ui->outputSender->setText("Could not connect to the database, check debug log");
        }
    }
    //Normal run state
    else{
        int currentStacked = ui->stackedWidget->currentIndex();
        switch (currentStacked) {
        //Start
            case 0:
                {
                    clearUI();
                    ui->pushBack->setText("Back");
                    switch (ui->comboBox->currentIndex()){
                    case 0 :
                    {
                        //Income
                        isExpenseGlobal = 0;
                        ui->progressBar->setValue(25);
                        ui->stackedWidget->setCurrentIndex(1);
                        ui->labelValue->setText("Value:");
                        ui->labelAddInfo1->setText("for income");
                        ui->labelAddInfo2->setText("for income");
                        ui->labelMonthly->setText("Is it a monthly income?");
                        break;
                    }
                    case 1 :
                    {
                        //Expense
                        isExpenseGlobal = 1;
                        ui->progressBar->setValue(25);
                        ui->stackedWidget->setCurrentIndex(1);
                        ui->labelValue->setText("Cost:");
                        ui->labelAddInfo1->setText("for expense");
                        ui->labelAddInfo2->setText("for expense");
                        ui->labelMonthly->setText("Is it a monthly expense?");
                        break;
                    }
                    case 2:
                    {
                        //Find budgets
                        ui->stackedWidget->setCurrentIndex(2);
                        std::pair<bool, QSqlQuery> output{budgetItem.findMonths(db)};
                        if(output.first == false || output.second.next() == false){
                            ui->outputSender->setText("Could not find any dates, check debug log");
                            qDebug() << "Send Data error:"
                                     << output.second.lastError()
                                     << "\n"
                                     << output.second.lastQuery()
                                     << "\n"
                                     << output.second.executedQuery();
                        }
                        else{
                            ui->monthBudgets->setEnabled(true);
                            QList<QString> budgetDates;
                            budgetDates.append(dateMonthAndYear(output.second.value(0).toString()));
                            QString dateFromDB;
                            while (output.second.next()) {
                                dateFromDB = dateMonthAndYear(output.second.value(0).toString());
                                if(budgetDates.indexOf(dateFromDB) == -1){
                                    budgetDates.append(dateFromDB);
                                }
                                else{
                                }
                            }
                            for(int i{0}; i < budgetDates.length(); i++){
                                ui->monthBudgets->addItem(budgetDates[i]);
                            }
                        }
                        break;
                    }
                    case 3:
                    {
                        //Find budgets for editing entries
                        int emptyMonthly{false};
                        int emptyMonths{false};
                        ui->stackedWidget->setCurrentIndex(4);
                        std::pair<bool, QSqlQuery> monthly{budgetItem.findMonthly(db)};
                        if(monthly.first == false || monthly.second.next() == false)
                        {
                            emptyMonthly = true;
                            qDebug() << "Send Data error:"
                                     << monthly.second.lastError()
                                     << "\n"
                                     << monthly.second.lastQuery()
                                     << "\n"
                                     << monthly.second.executedQuery();
                        }
                        std::pair<bool, QSqlQuery> months{budgetItem.findMonths(db)};
                        if(months.first == false || months.second.next() == false)
                        {
                            emptyMonths = true;
                            qDebug() << "Send Data error:"
                                     << months.second.lastError()
                                     << "\n"
                                     << months.second.lastQuery()
                                     << "\n"
                                     << months.second.executedQuery();
                        }
                        if(emptyMonths == true and emptyMonthly == true)
                        {
                            ui->outputSender->setText("Could not find any dates, check debug log");
                        }
                        else
                        {
                            ui->comboBoxEditMonths->setEnabled(true);
                            if(emptyMonthly == false)
                            {
                                ui->comboBoxEditMonths->addItem("Monthly");
                            }
                            if(emptyMonths == false)
                            {
                                QList<QString> addedMonths;
                                QString month;
                                std::pair<bool, QSqlQuery> output{budgetItem.findMonths(db)};
                                while(output.second.next())
                                {
                                    month = dateMonthAndYear(output.second.value(0).toString());
                                    if(addedMonths.indexOf(month) == -1)
                                    {
                                        addedMonths.append(month);
                                    }
                                    else;
                                }
                                addedMonths.sort();
                                foreach(QString qstr, addedMonths)
                                {
                                    ui->comboBoxEditMonths->addItem(qstr);
                                }
                            }
                        }
                        break;
                    }
                    }
                }
            break;

        case 1:
        {
            //Insert values
            switch(ui->addEntriesChild->currentIndex())
            {
            case 0:
            {
                if(ui->checkBoxCustomDate->isChecked() == true){
                    date = ui->dateEditDateAdd->date().toString("dd-MM-yyyy");
                }
                else date = qdate.toString("dd-MM-yyyy");
                ui->progressBar->setValue(50);
                ui->addEntriesChild->setCurrentIndex(1);
            }
                break;
            case 1:
            {
                check = true;
                checkList.append(checkName(ui->lineEditName->text()));
                checkList.append(checkValue(ui->lineEditValue->text()));

                for(bool i : checkList){
                    check = check && i;
                }

                if(check == true){
                    //DB handling
                    QString name{ui->lineEditName->text()};
                    double value{ui->lineEditValue->text().toDouble()};
                    if(isExpenseGlobal == 1)
                    {
                        if(value >= 0) budgetItem.makeItem(name, date, -value, ui->checkBoxMonthly->isChecked());
                        else budgetItem.makeItem(name, date, value, ui->checkBoxMonthly->isChecked());
                    }
                    else budgetItem.makeItem(name, date, value, ui->checkBoxMonthly->isChecked());
                    qDebug() << budgetItem.itemValues();
                    std::pair<bool, QSqlQuery> output = budgetItem.databaseInsert(db);
                    bool boolCheck = output.first;

                    if(boolCheck == true){
                        ui->stackedWidget->setCurrentIndex(0);
                        clearUI();
                        ui->outputSender->setText("Your data was successfully send to the database");
                        ui->pushBack->setText("Exit");
                    }
                    else{
                        ui->stackedWidget->setCurrentIndex(0);
                        clearUI();
                        ui->outputSender->setText("Could not send data to the database, check debug log");
                        qDebug() << "Send DRadioButtonata error:"
                                 << output.second.lastError()
                                 << "\n"
                                 << output.second.lastQuery()
                                 << "\n"
                                 << output.second.executedQuery();
                    }
                    //DB handling end
                }
                else{
                    ui->outputSender->clear();
                    int checkIndex{};
                    QString errorOutput;
                    while(checkList.indexOf(false) > -1){
                        checkIndex = checkList.indexOf(false);
                        switch (checkIndex) {
                        case 0:
                            errorOutput = "Please write a valid name.";
                            break;
                        case 1:
                            errorOutput = "Please write a valid value.";
                            break;
                        case 2:
                            errorOutput = "Please write a valid date.";
                            break;
                        }
                        checkList[checkIndex] = true;
                        ui->outputSender->append(errorOutput + "\n");
                    }
                    checkList.clear();
                }
            }
                break;
            }



        }
            break;

        case 2:
        {
            //Fetch budgets site
            //If there are no entries:
            if(ui->monthBudgets->isEnabled() == false)
            {
                ui->outputSender->setText("Failed: No entries in the database");
            }
            else
            {

                QString selected{dateISOConvertMonthAndYear(ui->monthBudgets->currentText())};
                std::pair<bool, QSqlQuery> fetchBudget = budgetItem.getData(db, (selected + "-01"), (selected + "-" + dateISODaysInMonth(selected)), 1);
                if(fetchBudget.first == true){

                    QStringList headers {"Name", "Day", "Value in Kr,-"};
                    QList<double> total;
                    ui->tableWidgetBudgetMain->setColumnCount(3);
                    ui->tableWidgetBudgetMain->setHorizontalHeaderLabels(headers);
                    ui->tableWidgetBudgetMonthly->setColumnCount(3);
                    ui->tableWidgetBudgetMonthly->setHorizontalHeaderLabels(headers);
                    ui->labelBudgetDate->setText(monthList[selected.section("-", 1, 1).toInt() - 1]);
                    //From main table
                    int row{0};
                    while (fetchBudget.second.next()) {
                        ui->tableWidgetBudgetMain->insertRow(row);
                        for(int i{0}; i < 3; i++){

                            switch (i)
                            {
                            case 0:
                                ui->tableWidgetBudgetMain->setItem(row, i, new QTableWidgetItem(fetchBudget.second.value(i).toString()));
                                break;
                            case 1:
                                ui->tableWidgetBudgetMain->setItem(row, i, new QTableWidgetItem((fetchBudget.second.value(i).toString()).section("-", 2, 2)));
                                break;
                            case 2:
                                ui->tableWidgetBudgetMain->setItem(row, i, new QTableWidgetItem(fetchBudget.second.value(i).toString()));
                                total.append(fetchBudget.second.value(i).toDouble());
                                break;
                            }
                        }
                        row++;
                    }
                    //From monthly table
                    fetchBudget = budgetItem.getData(db, (selected + "-01"), (selected + "-" + dateISODaysInMonth(selected)), 2);
                    row = 0;
                    while (fetchBudget.second.next()) {
                        ui->tableWidgetBudgetMonthly->insertRow(row);
                        for(int i{0}; i < 3; i++){

                            switch (i)
                            {
                            case 0:
                                ui->tableWidgetBudgetMonthly->setItem(row, i, new QTableWidgetItem(fetchBudget.second.value(i).toString()));
                                break;
                            case 1:
                                ui->tableWidgetBudgetMonthly->setItem(row, i, new QTableWidgetItem(fetchBudget.second.value(i).toString()));
                                break;
                            case 2:
                                ui->tableWidgetBudgetMonthly->setItem(row, i, new QTableWidgetItem(fetchBudget.second.value(i).toString()));
                                total.append(fetchBudget.second.value(i).toDouble());
                                break;
                            }
                        }
                        row++;
                    }
                    ui->tableWidgetBudgetMain->sortItems(2, Qt::AscendingOrder);
                    ui->tableWidgetBudgetMonthly->sortItems(2, Qt::AscendingOrder);
                    double totalResult{0};
                    for (int i{0}; i < total.length(); i++){
                        totalResult += total[i];
                    }
                    ui->lineEditBudgetTotal->setText(QString::number(totalResult));
                    ui->stackedWidget->setCurrentIndex(3);
                 }
                 else{
                     ui->stackedWidget->setCurrentIndex(0);
                     clearUI();
                     ui->outputSender->setText("Could not fetch data from the database, check debug log");
                     qDebug() << "Send Data error:"
                              << fetchBudget.second.lastError()
                              << "\n"
                              << fetchBudget.second.lastQuery()
                              << "\n"
                              << fetchBudget.second.executedQuery();
                 }
            }
        }
            break;
        case 4:
        {
            //Editing entries

            switch (ui->editEntriesChild->currentIndex()) {
            case 0:
            {
                //If there are no entries:
                if(ui->comboBoxEditMonths->currentText() == "Pick a month")
                {
                    ui->outputSender->setText("Pick a month");
                }
                else
                {
                    if(ui->comboBoxEditMonths->isEnabled() == false)
                    {
                        ui->outputSender->setText("Failed: No entries in the database");
                    }
                    else
                    {
                        if(ui->comboBoxEditMonths->currentText() == "Monthly")
                        {
                            ui->checkBoxEditMonthly->setChecked(true);
                            mode = 1;
                        }
                        else
                        {
                            ui->checkBoxEditMonthly->setChecked(false);
                            mode = 2;
                        }
                        std::pair<bool, QSqlQuery> getEntry{budgetItem.getEntry(db, idList[ui->comboBoxEditEntries->currentIndex()], mode)};
                        if(getEntry.first == true)
                        {
                            getEntry.second.next();
                            id = getEntry.second.value(0).toInt();
                            if(getEntry.second.value(3).toInt() >= 0 ) ui->comboBoxEditType->setCurrentIndex(0);
                            else ui->comboBoxEditType->setCurrentIndex(1);
                            ui->lineEditNameEdit->setText(getEntry.second.value(1).toString());
                            ui->lineEditValueEdit->setText(getEntry.second.value(3).toString());
                            QDate dateToDB = QDate::fromString(dateConvertFromISO(getEntry.second.value(2).toString()), "dd-MM-yyyy");
                            ui->dateEditDateEdit->setDate(dateToDB);
                            ui->editEntriesChild->setCurrentIndex(1);
                        }
                        else
                        {
                            ui->outputSender->setText("Could not fetch data from the database, check debug log");
                            qDebug() << "Send Data error:"
                                     << getEntry.second.lastError()
                                     << "\n"
                                     << getEntry.second.lastQuery()
                                     << "\n"
                                     << getEntry.second.executedQuery();
                        }
                    }
                }

                break;
            }
            case 1:
            {
                //Send data for updating
                int remove{0};
                QString dateToDB;
                QString name{ui->lineEditNameEdit->text()};
                QString expense{ui->comboBoxEditType->currentIndex()};
                std::pair<bool, QSqlQuery> updateEntry;
                std::pair<bool, QSqlQuery> removeEntry;
                double valueNumeric{ui->lineEditValueEdit->text().toDouble()};
                if(expense == 1)
                {
                    if(valueNumeric > 0) valueNumeric = -valueNumeric;
                    else;
                }
                else
                {
                    if(valueNumeric < 0) valueNumeric = -valueNumeric;
                    else;
                }
                QString value{QString::number(valueNumeric)};
                if(mode == 1)
                {
                    if(ui->checkBoxEditMonthly->isChecked() == true)
                    {
                        dateToDB = ui->dateEditDateEdit->date().toString("dd");
                        updateEntry = budgetItem.updateEntry(db, id, name, dateToDB, value, mode);
                    }
                    else
                    {
                        dateToDB = ui->dateEditDateEdit->date().toString("yyyy-MM-dd");
                        updateEntry = budgetItem.addEntry(db, id, name, dateToDB, value, 2);
                        removeEntry = budgetItem.removeEntry(db, id, mode);
                        remove = 1;
                    }

                }
                else if(ui->checkBoxEditMonthly->isChecked() == true)
                {
                    dateToDB = ui->dateEditDateEdit->date().toString("dd");
                    updateEntry = budgetItem.addEntry(db, id, name, dateToDB, value, 1);
                    removeEntry = budgetItem.removeEntry(db, id, mode);
                    remove = 1;
                }
                else
                {
                    dateToDB = ui->dateEditDateEdit->date().toString("yyyy-MM-dd");
                    updateEntry = budgetItem.updateEntry(db, id, name, dateToDB, value, 2);
                }

                //Error checking
                if(updateEntry.first == false)
                {
                    ui->outputSender->setText("Could not update entry in the database, check debug log");
                    qDebug() << "Send Data error:"
                             << updateEntry.second.lastError()
                             << "\n"
                             << updateEntry.second.lastQuery()
                             << "\n"
                             << updateEntry.second.executedQuery();
                }
                else if(removeEntry.first == false and remove == 1)
                {
                    ui->outputSender->setText("Could not delete entry in the database, check debug log");
                    qDebug() << "Send Data error:"
                             << removeEntry.second.lastError()
                             << "\n"
                             << removeEntry.second.lastQuery()
                             << "\n"
                             << removeEntry.second.executedQuery();
                }
                else
                {
                    clearUI();
                    ui->stackedWidget->setCurrentIndex(0);
                    ui->pushBack->setText("Exit");
                    ui->outputSender->setText("Successfully update entry");
                }
                break;
            }
            }

            break;
        }
        }
    }

}

void MainWindow::pushBack_released()
{
    switch (ui->stackedWidget->currentIndex())
    {
    case 0:
        db.close();
        close();
        break;
    case 1:
        //Add
        switch (ui->addEntriesChild->currentIndex()) {
        case 0:
            clearUI();
            ui->pushBack->setText("Exit");
            ui->stackedWidget->setCurrentIndex(0);
            break;
        case 1:
            ui->outputSender->clear();
            ui->addEntriesChild->setCurrentIndex(0);
            break;
        }
        break;
    case 2:
        clearUI();
        ui->stackedWidget->setCurrentIndex(0);
        ui->pushBack->setText("Exit");
        break;
    case 3:
        ui->tableWidgetBudgetMain->clear();
        ui->tableWidgetBudgetMonthly->clear();
        ui->stackedWidget->setCurrentIndex(2);
        ui->pushBack->setText("Back");
        break;
    case 4:
        //Edit
        switch (ui->editEntriesChild->currentIndex())
        {
        case 0:
            clearUI();
            ui->stackedWidget->setCurrentIndex(0);
            ui->pushBack->setText("Exit");
            break;
        case 1:
            ui->editEntriesChild->setCurrentIndex(0);
            ui->pushBack->setText("Back");
            break;
        }
        break;



    }
}
