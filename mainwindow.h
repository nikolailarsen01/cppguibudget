#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QSqlDatabase>
#include <QCheckBox>
#include <QMainWindow>
#include <QPushButton>
#include <QComboBox>
#include <QStackedWidget>
#include <QLineEdit>
#include <QLabel>
#include <QTextBrowser>
#include <QProgressBar>
#include <QTableWidget>
#include <QRadioButton>
#include <QDateEdit>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void pushNext_released();
    void pushBack_released();
    void pushButtonDeleteYes_released();
    void pushButtonDeleteNo_released();
    void checkBoxMonthly_released();
    void checkBoxCustomDate_released();
    void comboBoxEditMonths_activated();
    void checkBoxEditMonthly_stateChanged();

    void clearUI();
    void keyPressEvent(QKeyEvent *k);

private:
    Ui::MainWindow *ui;
    //Functions
    QString dateMonthAndYear(QString input);
    QString dateISOConvertMonthAndYear(QString input);
    QString dateISODaysInMonth(QString dateISO);
    QString dateConvertFromISO(QString input);

    //Variables
    int isExpenseGlobal;
    int counter;
    int id;

    bool check;

    QSqlDatabase db;

    QList<int> idList;
    QList<bool> checkList;

    //Main UI elements
    QPushButton *pushNext;
    QPushButton *pushBack;
    QPushButton *comboBox;

    QStackedWidget *stackedWidget;
    QStackedWidget *addEntriesChild;
    QStackedWidget *editEntriesChild;

    QTableWidget *tableWidget;
    QTextBrowser *outputSender;
    QProgressBar *progressBar;

    QLabel *labelTitle;


    //UI element belonging to Budgets
    QLabel *labelBudgetDate;

    QLineEdit *lineEditBudgetTotal;

    //UI elements belonging to addEntries
    QLabel *labelMonthly;
    QLabel *labelDate;
    QLabel *labelValue;
    QLabel *labelAddInfo1;
    QLabel *labelAddInfo2;
    QCheckBox *checkBoxMonthly;
    QCheckBox *checkBoxCustomDate;

    QLineEdit *lineEditName;
    QLineEdit *lineEditValue;

    QDateEdit *dateEditDateAdd;

    //UI elements belonging to editEntries
    QComboBox *comboBoxEditEntries;
    QComboBox *comboBoxEditMonths;
    QComboBox *comboBoxEditType;

    QLineEdit *lineEditNameEdit;
    QLineEdit *lineEditValueEdit;

    QCheckBox *checkBoxEditMonthly;

    QDateEdit *dateEditDateEdit;

    QLabel *labelDelete;

    QPushButton *pushButtonDeleteYes;
    QPushButton *pushButtonDeleteNo;

};
#endif // MAINWINDOW_H
