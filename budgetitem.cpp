#include "budgetitem.h"

#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QVariant>
#include <filesystem>


BudgetItem::BudgetItem()
{

}

//Main functions
void BudgetItem::makeItem(QString Name, QString Date, double Value, bool Monthly)
{
    name = Name;
    value = Value;
    date = (Date.section("-", 2, 2) + "-" + Date.section("-", 1, 1) + "-" + Date.section("-", 0, 0));
    monthly = Monthly;
}

QString BudgetItem::itemValues()
{
    return "The name of the item is: "+name+
            " The boolean for monthly is: "+QString::number(monthly)+" The value for the item is: "+QString::number(value)+" The date for the item is: "+date;
}

//Database connection stuff
QSqlDatabase BudgetItem::databaseConnection(QSqlDatabase db){
    db = QSqlDatabase::addDatabase("QPSQL");
    //QString path{QString::fromStdString(std::filesystem::current_path())};
    db.setDatabaseName("budget_db");
    db.setPort(5432);
    db.setHostName("localhost");
    db.setUserName("postgres");
    db.setPassword("admin");
    db.QSqlDatabase::open();
    return db;
}

std::pair<bool, QSqlQuery> BudgetItem::databaseInsert(QSqlDatabase db){
    QSqlQuery query(db);
    if(monthly == true)
    {
        query.prepare("INSERT INTO monthly (name, date, value) VALUES(:name, :date, :value)");
        query.bindValue(":name", name);
        query.bindValue(":date", date.section("-", 2, 2));
        query.bindValue(":value", value);
    }
    else
    {
        query.prepare("INSERT INTO main (name, date, value) VALUES(:name, :date, :value)");
        query.bindValue(":name", name);
        query.bindValue(":date", date);
        query.bindValue(":value", value);
    }

    return std::make_pair(query.exec(), query);
}

std::pair<bool, QSqlQuery> BudgetItem::findMonths(QSqlDatabase db){
    QSqlQuery query(db);
    query.prepare("SELECT date FROM main");
    return std::make_pair(query.exec(), query);
}

std::pair<bool, QSqlQuery> BudgetItem::findMonthly(QSqlDatabase db){
    QSqlQuery query(db);
    query.prepare("SELECT * FROM monthly");
    return std::make_pair(query.exec(), query);
}

std::pair<bool, QSqlQuery> BudgetItem::findEntries(QSqlDatabase db, QString DateStart, QString DateEnd){
    QSqlQuery query(db);
    query.prepare("SELECT * FROM main where date >= :dateStart AND date <= :dateEnd");
    query.bindValue(":dateStart", DateStart);
    query.bindValue(":dateEnd", DateEnd);
    return std::make_pair(query.exec(), query);
}

std::pair<bool, QSqlQuery> BudgetItem::findEntriesMonthly(QSqlDatabase db){
    QSqlQuery query(db);
    query.prepare("SELECT * FROM monthly");
    return std::make_pair(query.exec(), query);
}

std::pair<bool, QSqlQuery> BudgetItem::getEntry(QSqlDatabase db, int id, int mode){
    QSqlQuery query(db);
    if(mode == 1)
    {
        query.prepare("select * from monthly where id = :id");
    }
    else
    {
        query.prepare("select * from main where id = :id");
    }
    query.bindValue(":id", id);
    return std::make_pair(query.exec(), query);
}

std::pair<bool, QSqlQuery> BudgetItem::getDate(QSqlDatabase db, int id){
    QSqlQuery query(db);
    query.prepare("select day, month, year from main where id = :id");
    query.bindValue(":id", id);
    return std::make_pair(query.exec(), query);
}

std::pair<bool, QSqlQuery> BudgetItem::getData(QSqlDatabase db, QString DateStart, QString DateEnd, int mode){
    QSqlQuery query(db);
    if(mode == 1)
    {
        query.prepare("SELECT name, date, value FROM main where date >= :dateStart AND date <= :dateEnd");
        query.bindValue(":dateStart", DateStart);
        query.bindValue(":dateEnd", DateEnd);
    }
    else if(mode == 2)
    {
        query.prepare("SELECT name, date, value FROM monthly where date >= :dateStart AND date <= :dateEnd");
        query.bindValue(":dateStart", DateStart.section("-", 2, 2));
        query.bindValue(":dateEnd", DateEnd.section("-", 2, 2));
    }

    return std::make_pair(query.exec(), query);
}
std::pair<bool, QSqlQuery> BudgetItem::addEntry(QSqlDatabase db, int id, QString Name, QString Date, QString Value, int mode)
{
    QSqlQuery query(db);
    if(mode == 1)
    {
        query.prepare("INSERT INTO monthly (name, date, value) VALUES(:name, :date, :value)");
    }
    else if(mode == 2)
    {
        query.prepare("INSERT INTO main (name, date, value) VALUES(:name, :date, :value)");
    }
    query.bindValue(":name", Name);
    query.bindValue(":date", Date);
    query.bindValue(":value", Value);
    query.bindValue(":id", id);
    return std::make_pair(query.exec(), query);
}

std::pair<bool, QSqlQuery> BudgetItem::updateEntry(QSqlDatabase db, int id, QString Name, QString Date, QString Value, int mode)
{
    QSqlQuery query(db);
    if(mode == 1)
    {
        query.prepare("UPDATE monthly SET name = :name, date = :date, value = :value WHERE id = :id");
    }
    else if(mode == 2)
    {
        query.prepare("UPDATE main SET name = :name, date = :date, value = :value WHERE id = :id");
    }
    query.bindValue(":name", Name);
    query.bindValue(":date", Date);
    query.bindValue(":value", Value);
    query.bindValue(":id", id);
    return std::make_pair(query.exec(), query);
}

std::pair<bool, QSqlQuery> BudgetItem::removeEntry(QSqlDatabase db, int id, int mode){
    QSqlQuery query(db);
    if(mode == 1)
    {
        query.prepare("DELETE FROM monthly where id = :id");
    }
    else if (mode == 2)
    {
        query.prepare("DELETE FROM main where id = :id");
    }
    query.bindValue(":id", id);
    return std::make_pair(query.exec(), query);
}




