#ifndef BUDGETITEM_H
#define BUDGETITEM_H

#include <QString>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QVariant>
#include <filesystem>

class BudgetItem
{
public:
    BudgetItem();
    QString name;
    void makeItem(QString Name, QString Date, double Value, bool Monthly);
    QString itemValues();

    QSqlDatabase databaseConnection(QSqlDatabase db);
    std::pair<bool, QSqlQuery> databaseInsert(QSqlDatabase db);
    std::pair<bool, QSqlQuery> findMonths(QSqlDatabase db);
    std::pair<bool, QSqlQuery> findMonthly(QSqlDatabase db);
    std::pair<bool, QSqlQuery> findEntries(QSqlDatabase db, QString DateStart, QString DateEnd);
    std::pair<bool, QSqlQuery> findEntriesMonthly(QSqlDatabase db);
    std::pair<bool, QSqlQuery> getEntry(QSqlDatabase db, int id, int mode);
    std::pair<bool, QSqlQuery> getDate(QSqlDatabase db, int id);
    std::pair<bool, QSqlQuery> getData(QSqlDatabase db, QString DateStart, QString DateEnd, int mode);
    std::pair<bool, QSqlQuery> addEntry(QSqlDatabase db, int id, QString Name, QString Date, QString Value, int mode);
    std::pair<bool, QSqlQuery> updateEntry(QSqlDatabase db, int id, QString Name, QString Date, QString Value, int mode);
    std::pair<bool, QSqlQuery> removeEntry(QSqlDatabase db, int id, int mode);

private:
    bool monthly;
    double value;
    QString date;
};

#endif // BUDGETITEM_H
